package schibsted.coding.exercise.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import schibsted.coding.exercise.service.ISearchTextService;

public class SearchTextServiceTest {
	
	private static final String FILE22 = "file2";
	private static final String FILE12 = "file1";
	private static final String WORLD = "world";
	private static final String HELLO = "Hello";
	
	private static ISearchTextService searchTextService = new SearchTextService();
	
	@Test
	public void testGetFilesOccurencePercentage(){
		List<String> helloWordsToSearch = new ArrayList<>();
		helloWordsToSearch.add(HELLO);
		
		List<String> worldWordsToSearch = new ArrayList<>();
		worldWordsToSearch.add(WORLD);
		
		List<String> bothWordsToSearch = new ArrayList<>();
		bothWordsToSearch.add(HELLO);
		bothWordsToSearch.add(WORLD);
		
		Map<String, Map<String, Integer>> emptyInmemoryFiles = new HashMap<>();
		
		Map<String, Map<String, Integer>> emptyContentInmemoryFiles = new HashMap<>();
		Map<String, Integer> file1 = new HashMap<>();
		emptyContentInmemoryFiles.put(FILE12, file1);
		Map<String, Integer> file2 = new HashMap<>();
		emptyContentInmemoryFiles.put(FILE22, file2);
		
		Map<String, Map<String, Integer>> inmemoryFiles = new HashMap<>();
		file1 = new HashMap<>();
		file1.put(HELLO, 1);
		file1.put(WORLD, 1);
		inmemoryFiles.put(FILE12, file1);
		file2 = new HashMap<>();
		file2.put(HELLO, 1);
		inmemoryFiles.put(FILE22, file2);
		
		Map<String, Double> filesOccurencePercentage1 = searchTextService.getFilesOccurencePercentage(emptyInmemoryFiles, bothWordsToSearch);
		assertEquals(0, filesOccurencePercentage1.size());
		
		Map<String, Double> filesOccurencePercentage2 = searchTextService.getFilesOccurencePercentage(emptyContentInmemoryFiles, bothWordsToSearch);
		assertEquals(2, filesOccurencePercentage2.size());
		Collection<Double> values2 = filesOccurencePercentage2.values();
		assertEquals(true, values2.contains(new Double(0.0)));
		assertEquals(false, values2.contains(new Double(100.0)));
		assertEquals(false, values2.contains(new Double(50.0)));

		Map<String, Double> filesOccurencePercentage3 = searchTextService.getFilesOccurencePercentage(inmemoryFiles, bothWordsToSearch);
		assertEquals(2, filesOccurencePercentage3.size());
		Collection<Double> values3 = filesOccurencePercentage3.values();
		assertEquals(true, values3.contains(new Double(100.0)));
		assertEquals(true, values3.contains(new Double(50.0)));

		Map<String, Double> filesOccurencePercentage4 = searchTextService.getFilesOccurencePercentage(inmemoryFiles, helloWordsToSearch);
		assertEquals(2, filesOccurencePercentage4.size());
		Collection<Double> values4 = filesOccurencePercentage4.values();
		assertEquals(true, values4.contains(new Double(100.0)));
		assertEquals(false, values4.contains(new Double(50.0)));
		assertEquals(false, values4.contains(new Double(00.0)));

		Map<String, Double> filesOccurencePercentage5 = searchTextService.getFilesOccurencePercentage(inmemoryFiles, worldWordsToSearch);
		assertEquals(2, filesOccurencePercentage5.size());
		Collection<Double> values5 = filesOccurencePercentage5.values();
		assertEquals(true, values5.contains(new Double(100.0)));
		assertEquals(true, values5.contains(new Double(0.0)));
		
	}
}
