package schibsted.coding.exercise.service.impl;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import schibsted.coding.exercise.service.ISearchTextService;

import java.util.TreeMap;

/**
 * Implementation of the {@link ISearchTextService} interface.
 * 
 * @author C316285
 *
 */
public class SearchTextService implements ISearchTextService {

	@Override
	public Map<String, Double> getFilesOccurencePercentage(Map<String, Map<String, Integer>> inmemoryFiles,
			List<String> wordsToSearch) {
		
		Map<String, Double> filesOccurence = new HashMap<>();

		for (Entry<String, Map<String, Integer>> entry : inmemoryFiles.entrySet()){
			String name = entry.getKey();
			Map<String, Integer> fileContent = entry.getValue();
			int wordsFoundInFile = 0;
			for (String word : wordsToSearch){
				if (fileContent.containsKey(word)){
					wordsFoundInFile++;
				}
			}
			
			Double fileOccurencePercentage = cumputeFileOccurencePercentage(wordsFoundInFile, wordsToSearch);
			filesOccurence.put(name, fileOccurencePercentage);
		}
		
		Map<String, Double> sortedFilesOccurence = sortByValues(filesOccurence);
		return sortedFilesOccurence;
	}

	/**
	 * This the ranking score design based on the number of the given words founded in the file.
	 * 
	 * @param wordsFoundInFile no of words founded in the file
	 * @param wordsToSearch list of words searched in the file
	 * @return the ranking score of percentage occurrences
	 */
	private Double cumputeFileOccurencePercentage(int wordsFoundInFile, List<String> wordsToSearch) {
		int wordsToSearchSize = wordsToSearch.size();
		Double occurencePercentage = (wordsFoundInFile * 100.00)/wordsToSearchSize;
		return occurencePercentage;
	}

	/**
	 * This is a method used to sort a {@link TreeMap} by values by implementing comparator interface.
	 * It is used to store percentage occurrences in a {@link TreeMap} (sorted map) in order to have them
	 * in an sorted descending order to print only top 10 results.
	 * 
	 * @param map of percentage occurrences in a random order.
	 * @return a sorted {@link TreeMap} of percentage occurrences.
	 */
	private Map<String, Double> sortByValues(final Map<String, Double> map) {
		Comparator<String> valueComparator = new Comparator<String>() {
			public int compare(String s1, String s2) {
				int compare = map.get(s2).compareTo(map.get(s1));
				if (compare == 0) {
					return 1;
				} else {
					return compare;
				}
			}
		};

		Map<String, Double> sortedByValues = new TreeMap<String, Double>(valueComparator);
		sortedByValues.putAll(map);
		return sortedByValues;
	}

}
