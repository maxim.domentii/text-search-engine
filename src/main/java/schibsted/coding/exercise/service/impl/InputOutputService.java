package schibsted.coding.exercise.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import schibsted.coding.exercise.service.IInputOutputService;

/**
 * Implementation of the {@link IInputOutputService} interface.
 * 
 * @author C316285
 *
 */
public class InputOutputService implements IInputOutputService {
	
	private final Log LOGGER = LogFactory.getLog(getClass());
	
	// According to https://docs.oracle.com/javase/6/docs/api/java/util/regex/Pattern.html: A non-word character (anything else then [a-zA-Z_0-9])
	private static final String SPLIT_TOKENS = "\\W+"; 
	private static final String NO_MATCHES_FOUND = "no matches found";
	
	@Override
	public void printResults(Map<String, Double> occurencePercentages) {
		int i = 0;
		boolean found = false;
		for (Entry<String, Double> entry : occurencePercentages.entrySet()){
			if (i == 10) {
				break;
			}
			if (entry.getValue() > 0) {
				found = true;
				System.out.println(entry.getKey() + " : " + String.format("%.2f", entry.getValue()) + "%");
			}
		}
		
		if (!found){
			System.out.println(NO_MATCHES_FOUND);
		}
	}

	@Override
	public Map<String, Map<String, Integer>> getInmemoryFilesContent(File file) throws IOException {
		Map<String, Map<String, Integer>> inmemoryFiles = new HashMap<>();
		
		List<File> directoryFiles = new ArrayList<>(Arrays.asList(file.listFiles()));
		for (File directoryFile : directoryFiles){
			if (directoryFile.isFile()){
				try {
					Map<String, Integer> fileContent = getFileContent(directoryFile);
					inmemoryFiles.put(directoryFile.getName(), fileContent);
				} catch (IOException e) {
					LOGGER.error("IOException: Can't read file content of file " + directoryFile.getName());
					throw e;
				}
			}
		}
		
		return inmemoryFiles;
	}

	/**
	 * Takes a file as input and return the file content as a {@link Map} of words with occurrences in the current file.
	 * 
	 * @param directoryFile is a file to be read.
	 * @return a {@link Map} of words with occurrences in the current file.
	 * @throws IOException
	 */
	private Map<String, Integer> getFileContent(File directoryFile) throws IOException {
		Map<String, Integer> fileContent = new HashMap<>();
		
		BufferedReader br = new BufferedReader(new FileReader(directoryFile));
		String line = br.readLine();
		while (line != null){
			processLine(fileContent, line);
			line = br.readLine();
		}
		br.close();
		
		return fileContent;
	}

	/**
	 * This method take the map representation of the file and a line read from the file.
	 * Split given line in words and store them in the file content map.
	 * 
	 * @param fileContent map representation of the file content.
	 * @param line line from the file to be processed.
	 */
	private void processLine(Map<String, Integer> fileContent, String line) {
		List<String> words = new ArrayList<>(Arrays.asList(line.split(SPLIT_TOKENS)));
		for (String word : words){
			if (fileContent.containsKey(word)){
				int wordCount = fileContent.get(word);
				fileContent.put(word, wordCount+1);
			} else {
				fileContent.put(word, 1);
			}
		}
	}

}
