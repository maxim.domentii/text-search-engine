package schibsted.coding.exercise.service;

import java.util.List;
import java.util.Map;

/**
 * Interface used to perform the logic related to searching words in inmemory files.
 * 
 * @author C316285
 *
 */
public interface ISearchTextService {

	/**
	 * This method takes inmemory files and list of words from STDIN and return the percentage
	 * of occurrence of words in those files.
	 * 
	 * @param inmemoryFiles are files from the given directory stored inmemory.
	 * @param wordsToSearch is the list of words to search in the given list of files.
	 * @return a map representing the percentage of occurrence of words in the given files.
	 */
	Map<String, Double> getFilesOccurencePercentage(Map<String, Map<String, Integer>> inmemoryFiles,
			List<String> wordsToSearch);

}
