package schibsted.coding.exercise.service;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Interface used to perform the logic related to the read of files inmemory and printing results to STDOUT.
 * 
 * @author C316285
 *
 */
public interface IInputOutputService {

	/**
	 * This method takes a directory as input and store each file from directory as a {@link Map} as follow:
	 * <br>
	 * For each word in the file
	 * <br>
	 * - key is the word
	 * <br>
	 * - value is the number of occurrences in the current file.
	 *  
	 * @param file is the directory where to search.
	 * @return a {@link Map} of maps for each file where key is file name and value is the content map.
	 * @throws IOException
	 */
	Map<String, Map<String, Integer>> getInmemoryFilesContent(File file) throws IOException;
	
	/**
	 * This method takes a {@link Map} of percentage occurrences and print first 10 entries descended order.
	 * @param occurencePercentages is a {@link Map} of percentage occurrences
	 */
	void printResults(Map<String, Double> occurencePercentages);
	
}
