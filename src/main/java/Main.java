import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import schibsted.coding.exercise.service.IInputOutputService;
import schibsted.coding.exercise.service.ISearchTextService;
import schibsted.coding.exercise.service.impl.InputOutputService;
import schibsted.coding.exercise.service.impl.SearchTextService;

public class Main {
	
	private static final String SPLIT_TOKENS = " ";
	private static final String QUIT_COMMAND = ":quit";
	
	private static IInputOutputService inputOutputService = new InputOutputService();
	private static ISearchTextService searchTextService = new SearchTextService();

	public static void main(String[] args) throws IOException {
		if (args.length == 0 ) {
			throw new IllegalArgumentException("No directory given to index.");
		}
		final File indexableDirectory = new File(args[0]);
		if (!indexableDirectory.isDirectory()) {
			throw new IllegalArgumentException("Given argument is not a valid directory path.");
		}
		
		// Index all files in indexableDirectory
		Map<String, Map<String, Integer>> inmemoryFiles = inputOutputService.getInmemoryFilesContent(indexableDirectory);
		
		Scanner keyboard = new Scanner(System.in);
		
		while ( true ) {
			System.out.print("\nsearch> ");
			final String line = keyboard.nextLine().trim();
			if (QUIT_COMMAND.equals(line)){
				keyboard.close();
				return;
			} else if (line.length() == 0){
				continue;
			}
			
			// Search indexed files for words in line
			List<String> wordsToSearch = new ArrayList<>(Arrays.asList(line.trim().split(SPLIT_TOKENS))); 
			Map<String, Double> occurencePercentages = searchTextService.getFilesOccurencePercentage(inmemoryFiles, wordsToSearch);
			inputOutputService.printResults(occurencePercentages);			
			
		}
	}
}