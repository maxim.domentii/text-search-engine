Building and running
====================

In eclipse:

1. Import project in eclipse as Maven Project
2. Create a maven run configuration with goals "clean package" to build jar
3. Create a maven run configuration with goals "clean test" to tu run unit tests
4. Build using configured run config.
5. Go in project_root/target folder
6. run in CMD: java -jar SimpleSearch.jar dir_path
7. use it as in pdf assignment example

In CMD (maven configured in env vars is required):

1. Go in the project_roor directory
2. Run "mvn clean package" to build jar
3. Run "mvn clean test" to run only unit test
4. Go in project_root/target folder
5. run: java -jar SimpleSearch.jar dir_path
6. use it as in pdf assignment example